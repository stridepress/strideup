extern crate actix_web;
extern crate futures;
extern crate ipfs_api;
#[macro_use]
extern crate serde_derive;
extern crate actix_multipart;
extern crate http;
extern crate rsgen;

use actix_multipart::{Field, Multipart, MultipartError};
use actix_web::{
    error, middleware, web, App, Error, HttpRequest, HttpResponse, HttpServer, Result,
};
use futures::future::{err, Either, Future};
use futures::Stream;
use ipfs_api::IpfsClient;
use rsgen::{gen_random_string, OutputCharsType};
use std::cell::Cell;
use std::fs;
use std::io::Cursor;
use std::io::Write;

#[derive(Deserialize)]
pub struct FormData {
    content: String,
}

fn index() -> HttpResponse {
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(
            r#"
        <html>
            <header>
            </header>
            <body>
                <form action="/upload" method="post" enctype="multipart/form-data">
                    <input type="file" name="content">
                    <input type="submit" value="Submit">
                </form>
            </body>
        </html>
    "#,
        )
}

pub fn save_file(field: Field) -> impl Future<Item = String, Error = Error> {
    let output_chars_type = OutputCharsType::LatinAlphabetAndNumeric {
        use_upper_case: true,
        use_lower_case: true,
    };
    let file_path_string = format!("uploads/{}", gen_random_string(32, output_chars_type));
    let file = match fs::File::create(file_path_string.clone()) {
        Ok(file) => file,
        Err(e) => return Either::A(err(error::ErrorInternalServerError(e))),
    };
    Either::B(
        field
            .fold((file, 0i64), move |(mut file, mut acc), bytes| {
                // fs operations are blocking, we have to execute writes
                // on threadpool
                web::block(move || {
                    file.write_all(bytes.as_ref()).map_err(|e| {
                        println!("file.write_all failed: {:?}", e);
                        MultipartError::Payload(error::PayloadError::Io(e))
                    })?;
                    acc += bytes.len() as i64;
                    Ok((file, acc))
                }).map_err(|e: error::BlockingError<MultipartError>| match e {
                    error::BlockingError::Error(e) => e,
                    error::BlockingError::Canceled => MultipartError::Incomplete,
                })
            })
            .map(|(_, acc)| acc)
            .map_err(|e| {
                println!("save_file failed, {:?}", e);
                error::ErrorInternalServerError(e)
            })
            .then(|_| Ok(file_path_string)),
    )
}

pub fn upload_post(multipart: Multipart) -> impl Future<Item = HttpResponse, Error = Error> {
    let upload_future = multipart
        .map_err(error::ErrorInternalServerError)
        .map(|field| save_file(field).into_stream())
        .flatten()
        .collect()
        .map_err(|e| {
            println!("failed: {}", e);
            e
        });

    let cont = "test";
    let data = Cursor::new(cont);
    upload_future.then(|p| {
        let client = IpfsClient::new("ipfsapi.stride.press", 80).unwrap();
        let name = &p.ok().unwrap()[0];
        client
            .add_path(name)
            .map(|res| res.hash)
            .then(move |hash| {
                let h: String = hash.ok().unwrap();
                client.pin_add(&h, true);
                Ok(h)
            })
            .then(|h: Result<String>| {
                let n = h.ok().unwrap();
                fs::remove_file(&n);
                Ok(HttpResponse::MovedPermanently()
                    .header(
                        http::header::LOCATION,
                        format!("https://ipfs.stride.press/{}", n),
                    )
                    .finish())
            })
    })
}

fn main() {
    HttpServer::new(|| {
        App::new()
            .wrap(middleware::Logger::default())
            .service(web::resource("/upload").route(web::post().to_async(upload_post)))
            .service(web::resource("/").route(web::get().to(index)))
    }).bind("127.0.0.1:8088")
        .unwrap()
        .run();
}
